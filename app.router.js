'use strict';

angular
    .module('application')
    .config(appRouter);

appRouter.$inject = ['$stateProvider', '$urlRouterProvider'];

function appRouter($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('home', {
            url: '/',
            views: {
                'conteudo': {
                    controller: 'HomeController as home',
                    templateUrl: '/home/home.template.html'
                },
                'footer': {
                    templateUrl: '/layout/footer.html'
                },
            }
        });
};
