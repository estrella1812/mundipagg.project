'use strict';

angular
    .module('application', [
        'ngStorage', 'ngTouch', 'ngAnimate',  'ui.router', 'chart.js', 'angularSpinner'
    ]);
