(function () {
    'use strict';

    angular
        .module('application')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['usSpinnerService', 'RepositoryService'];

    function HomeController(usSpinnerService, RepositoryService) {
        var vm = this;

        // variaveis
        vm.errorRepo = false;
        vm.repos = {};
        vm.selectedRepo = {};

        // funções
        vm.getRepository = getRepository

        function _createChart(commits) {
            var arrayData = []
            var months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
            var actualDate = new Date();
            var labels = [];

            angular.forEach(commits, function(v, k) {
                var commitDate = new Date(commits[k].commit.author.date);
                var label = months[commitDate.getMonth()] + '/' + commitDate.getFullYear().toString();
                if (labels.indexOf(label) < 0) {
                    labels.unshift(label);
                    arrayData.unshift(1);
                } else {
                    arrayData[0] = arrayData[0] + 1;
                }
            });

            var data = [];
            data.push(arrayData);

            _makeGraph(labels, data);
        }

        function _getCommits(url) {
            RepositoryService.getCommits(url).then(
                function (result) {
                    usSpinnerService.stop('spinner-content');
                    vm.errorRepo = false;
                    if (!result.message) {
                        _createChart(result);
                    } else {
                        vm.errorRepo = true;
                    }
                },
                function (error) {
                    console.error(error)
                }
            )
        }

        _getRepositories();
        function _getRepositories() {
            RepositoryService.getRepositories().then(
                function (result) {
                    vm.repos = result;
                    vm.selectRepo = vm.repos[0].name;
                    getRepository();
                },
                function (error) {
                    console.error(error)
                }
            )
        }


        function _makeGraph(labels, data) {
            vm.labels = labels;
            vm.data = data;
            vm.series = ['Commits'];
            vm.options = {
                animation: false
            };
            vm.legend = true
        }

        function getRepository() {
            usSpinnerService.spin('spinner-content');
            RepositoryService.getRepository(vm.selectRepo).then(
                function (result) {
                    vm.selectedRepo = result;
                    _getCommits(vm.selectedRepo.commits_url);
                },
                function (error) {
                    console.error(error)
                }
            )
        }
    };
})();