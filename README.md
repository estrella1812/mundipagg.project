# Projeto Mundipagg GitHub API v3

Projeto feito com o design baseado totalmente em Bootstrap, todo o projeto foi criado em cima de uma estrutura pessoal pra projetos em AngularJS, 
por esse motivo, pode haver algumas coisas que não estão sendo usadas no SASS. Versão do AngularJS utilizada no projeto: 1.6.1

Para gerar a build pra produção, basta usar no terminal o comando: gulp build.
Ele irá gerar uma pasta chamada dist, com todos os arquivos necessários minificados e prontos para subir pra produção.

Desenvolvido por: Carlos Alberto Estrella Junior

[Link pro Linkedin](https://www.linkedin.com/in/carlos-estrella-095789a4/)