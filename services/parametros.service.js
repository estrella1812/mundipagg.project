(function () {
    'use strict';

    angular
        .module('application')
        .service('ParametrosService', ParametrosService);

    ParametrosService.$inject = ['$location'];

    function ParametrosService($location) {
        var vm = this;
        vm.parametros = $location.search();

        vm.analytics = {
            'utm_campaign': vm.parametros.utm_campaign,
            'utm_content': vm.parametros.utm_content,
            'utm_medium': vm.parametros.utm_medium,
            'utm_source': vm.parametros.utm_source,
            'utm_terms': vm.parametros.utm_terms
        };
    };
})();
