(function () {
    'use strict';

    angular
        .module('application')
        .service('ConfigurationService', ConfigurationService);

    ConfigurationService.$inject = [];

    function ConfigurationService() {
        var user = 'mundipagg';

        this.getRepositoriesUrl = 'https://api.github.com/users/'+ user +'/repos';
        this.getRepositoryUrl = 'https://api.github.com/repos/'+ user +'/{repository-name}';
    };
})();
