(function () {
    'use strict';

    angular
        .module('application')
        .service('RepositoryService', RepositoryService);

    RepositoryService.$inject = ['$http', 'ConfigurationService'];

    function RepositoryService($http, ConfigurationService) {
        var vm = this;
        var getRepositoriesUrl = ConfigurationService.getRepositoriesUrl;
        var repo = {};

        vm.getRepositories = getRepositories;
        vm.getRepository = getRepository;
        vm.getContrib = getContrib;
        vm.getCommits = getCommits;

        function getRepositories() {
            return $http.get(getRepositoriesUrl).then(
                function (result) {
                    return result.data;
                },
                function (error) {
                    console.error(error);
                }
            )
        }

        function getRepository(name) {
            var getRepositoryUrl = ConfigurationService.getRepositoryUrl;
            getRepositoryUrl = getRepositoryUrl.replace('{repository-name}', name);

            return $http.get(getRepositoryUrl).then(
                function (result) {
                    repo = result.data;
                    getContrib(repo.contributors_url);
                    return repo;
                },
                function (error) {
                    console.error(error);
                }
            )
        }

        function getContrib(url) {
            $http.get(url).then(
                function (result) {
                    repo.contrib_count = result.data.length;
                },
                function (error) {
                    console.error(error);
                }
            )
        }

        function getCommits(url) {
            url = url.replace('{/sha}', '');
            return $http.get(url).then(
                function (result) {
                    return result.data;
                },
                function (error) {
                    return error.data;
                }
            )
        }
    };
})();
